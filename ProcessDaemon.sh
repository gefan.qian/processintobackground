#!/bin/bash


# set -x



Eof()
{
cat << EOF

#!/bin/bash

while :
do

	ps -xo pid,user,comm,etimes > /tmp/processList  

	numberOfLines=\$(wc -l /tmp/processList)	

	numberOfLines=\$(echo \$numberOfLines | awk '{ print \$1 }')	
	echo "The total number of lines is: \$numberOfLines "	

	for ((lineNumber=2;lineNumber<=numberOfLines;lineNumber+=1))
	do								
		line=\$(echo \$(sed "\${lineNumber}q;d" /tmp/processList))	
		echo "The line number \${lineNumber} is written as: \$line "	
		seconds=\$(echo \$line | awk  '{ print \$NF }')		
		echo "The amount of seconds is: \$seconds "	
		if  [[ \$seconds -gt 59 ]]			
		then
			echo "\$seconds is greater than 59"		
			pid=\$(echo \$line | awk  '{ print \$1 }')		
			echo "The PID is \$pid "				
			kill -STOP \$pid
			kill -CONT \$pid
		fi

	done

	sleep 65
done
EOF
}




if ! find . -maxdepth 1 | grep ProcessBackground.sh
then
touch ProcessBackground.sh
Eof > ProcessBackground.sh
fi


	
sh ProcessBackground.sh	&